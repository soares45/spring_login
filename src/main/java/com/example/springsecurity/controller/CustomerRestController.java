package com.example.springsecurity.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.springsecurity.models.Customer;
import com.example.springsecurity.repositories.CustomerRepository;

@RestController
public class CustomerRestController {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@GetMapping("/customers")
	public Iterable<Customer> getCustomers() {
		return this.customerRepository.findAll();
	}
	
	@GetMapping("/customers/{customerId}")
	public Optional<Customer> getCustomer(@PathVariable int customerId) {
		return this.customerRepository.findById(customerId);
	}
	
	@PostMapping("/customers")
	public Customer addCustomer(@RequestBody Customer customer) {
		customer.setId(0); // For saveOrUpdate, we need to specify the id to 0 to save a new Customer
		this.customerRepository.save(customer);
		return customer;
	}
	
	@PutMapping("/customers/{customerId}")
	public Customer updateCustomer(@PathVariable int customerId, @RequestBody Customer customer) {
		customer.setId(customerId);
		this.customerRepository.save(customer);
		return customer;
	}
	
	@DeleteMapping("/customers/{customerId}")
	public String deleteCustomerById(@PathVariable int customerId) throws Exception {
		Optional<Customer> customer = this.customerRepository.findById(customerId);
		if (customer == null) {
			throw new Exception("Customer id not found - " + customerId);
		}
		this.customerRepository.deleteById(customerId);
		return "Deleted customer with id - " + customerId;
	}
	
}

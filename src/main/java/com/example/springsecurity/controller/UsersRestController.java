package com.example.springsecurity.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.springsecurity.models.Users;
import com.example.springsecurity.repositories.UsersRepository;

@RestController
public class UsersRestController {
	
	@Autowired
	private UsersRepository usersRepository;
	
	@GetMapping("/users")
	public Iterable<Users> getUsers() {
		return this.usersRepository.findAll();
	}
	
	@GetMapping("/users/{userId}")
	public Optional<Users> getUser(@PathVariable int userId) {
		return this.usersRepository.findById(userId);
	}
	
	@PostMapping("/users")
	public Users addUser(@RequestBody Users user) {
		user.setId(0); // For saveOrUpdate, we need to specify the id to 0 to save a new Customer
		this.usersRepository.save(user);
		return user;
	}
	
	@PutMapping("/users/{userId}")
	public Users updateCustomer(@PathVariable int userId, @RequestBody Users user) throws Exception {
		user.setId(userId);
		this.usersRepository.save(user);
		return user;
	}
	
	@DeleteMapping("/users/{userId}")
	public String deleteCustomerById(@PathVariable int userId) throws Exception {
		Optional<Users> user = this.usersRepository.findById(userId);
		if (user == null) {
			throw new Exception("User id not found - " + userId);
		}
		this.usersRepository.deleteById(userId);
		return "Deleted user with id - " + userId;
	}

}

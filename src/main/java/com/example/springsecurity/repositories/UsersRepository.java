package com.example.springsecurity.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.springsecurity.models.Users;

public interface UsersRepository extends CrudRepository<Users, Integer> {
	
	Users findByUsername(String username);

}
